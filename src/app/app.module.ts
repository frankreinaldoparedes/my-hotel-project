// Angular imports
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Routing
import { AppRoutingModule } from './app-routing.module';

// Components
import { AppComponent } from './app.component';

// Modules
import { LayoutModule } from './layout/layout.module';
import { SharedModule } from './shared/shared.module';

import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    // Angular imports
    BrowserAnimationsModule,
    BrowserModule,
    
    // Routing
    AppRoutingModule,
    
    // Modules
    LayoutModule,
    SharedModule,

    ToastrModule.forRoot(),

  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
