// Angular imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing
import { RegisterRoutingModule } from './register-routing.module';

// Components
import { RegisterComponent } from './register-component/register.component';

// Shared Module
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    RegisterComponent
  ],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    SharedModule
  ]
})
export class RegisterModule { }
