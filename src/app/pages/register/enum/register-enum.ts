export enum RegisterEnum {
    // HTML
    haveAnAccount = 'I already have an account',
    
    // TS
    componentName = 'Register',
    tabPrefixName = 'My Hotel - ',
    invalidForm = 'Form not valid',
    successMessage = 'The user was succesfully created',
    userExistMessage = 'The user already exists',
    signInRoute = 'sign-in',

}