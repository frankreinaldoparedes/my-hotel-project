// Angular imports
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

// Tab Title
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

// Rxjs
import { Subject, takeUntil } from 'rxjs';

// Services
import { UsersService } from 'src/app/core/users/users.service';
import { SharedService } from 'src/app/shared/shared.service';
import { ToastrService } from 'ngx-toastr';

// Types
import { User } from 'src/app/shared/types/user.type';

// Enum
import { RegisterEnum as Enum } from '../enum/register-enum';



@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styles: ['button { background-color: #55b0d6; color: #fff; }']
})
export class RegisterComponent implements OnInit, AfterViewInit, OnDestroy {

  // Subscriptions
  private _unsubscribeAll: Subject<any> = new Subject<any>();

  // Reactive forms
  formGroup: FormGroup;

  // Enum
  enum = Enum;

  // Current  
  users: User[] = [];


  constructor(
    private _sharedService: SharedService,
    private _usersService: UsersService,
    private formBuilder: FormBuilder,
    private toast: ToastrService,
    private _router: Router,
    private title: Title,
  ) { 
    this.formGroup = this.formBuilder.group({})
  }



  /* ** */
  /* Life Cycles */
  /* ** */

  ngOnInit (): void {
    // Set the title of the current tab
    this.title.setTitle(`${Enum.tabPrefixName}${Enum.componentName}`);

    this.initForm();

    this._usersService.users
    .pipe(
      takeUntil<User[]> (this._unsubscribeAll),
    )
    .subscribe({
      next: (users: User[]) => {
        this.users = users; 
      }
    });
  }

  ngAfterViewInit (): void {
    // Set the value of the Observable
    this._sharedService.formTitleValue = Enum.componentName;
  }

  ngOnDestroy (): void {
    this._unsubscribeAll.next(null);

    this._unsubscribeAll.complete();
  }



  /* ** */
  /* Component Functions */
  /* ** */
  
  // Init the reactive form group
  initForm (): void {
    this.formGroup = this.formBuilder.group( {
      name: [null, Validators.required],
      lastName: [null],
      email: [null, [
        Validators.required,
        Validators.email
      ]],
      password: [null, Validators.required],
    });
  }

  // Method that validate the information in the inputs
  // To save the information
  onSubmit (): void {
    if ( this.formGroup.valid ) {
      const finalUser: User = this.transformData();

      this.saveInformation(finalUser);
    }    
    else {
      this.toast.warning(Enum.invalidForm);

      this.formGroup.markAllAsTouched();
    }
  }

  // Transform de data from the reactive form into an object to be saved
  transformData (): User {
    let user: User = this._usersService.createEmptyUser();

    Object.keys(this.formGroup.controls).map( (controlName: string) => {
      const control: AbstractControl | null = this.formGroup.get(controlName);

      if ( control?.dirty ) {
        user[controlName as keyof User] = control.value;
      }
    });

    return user;
  }


  // Verify if an user with the email entered was created
  // if it was not created then create a new user
  // otherwise show a error message
  async saveInformation (user: User): Promise<void> {
    let userExist: boolean = false;

    await this._usersService.userExist(user.email)
    .pipe(
      takeUntil<boolean> (this._unsubscribeAll),
    )
    .subscribe({
      next: (exist: boolean) => userExist = exist 
    });

    if( !userExist ) {
      this.createUser(user);
    }
    else {
      this.toast.error(Enum.userExistMessage);
    }
  }


  // Add a user to the array of users and share
  // new array of user for the observable
  createUser (user: User): void {
    let newUsers: User[] = [...this.users];

      
    user = this.setUserID(newUsers, user);

    newUsers.push(user);

    this._usersService.usersValue = newUsers;


    this.toast.success(Enum.successMessage);

    this._router.navigate([Enum.signInRoute]);
  }


  // Set the id of the user
  setUserID (newUsers: User[], user: User): User {
    if (newUsers.length === 0) {
      user.id = 1;
    }
    else {
      const index: number = newUsers.length - 1;

      user.id = this.getNewID( newUsers[index].id );
    }

    return user;
  }

  // Return a new ID
  getNewID (currentID: number | undefined): number {
    return currentID! + 1;
  }

  // If the field has error return true, otherwise return false
  fieldHasErrors (controlName: string): boolean | undefined {
    return this.formGroup.get(controlName)?.touched &&  this.formGroup.get(controlName)?.invalid;
  }

}
