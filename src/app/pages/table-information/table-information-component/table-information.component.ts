// Angular imports
import { Component, OnDestroy, OnInit } from '@angular/core';

// Types
import { FormBuilder, FormGroup } from '@angular/forms';
import { User } from 'src/app/shared/types/user.type';
import { Title } from '@angular/platform-browser';

// Enum
import { TableInformationEnum as Enum } from '../enum/table-information-enum';

// Services
import { UsersService } from 'src/app/core/users/users.service';

// Rxjs
import { Subscription } from 'rxjs';


@Component({
  selector: 'table-information',
  templateUrl: './table-information.component.html',
  styleUrls: ['./table-information.component.scss']
})
export class TableInformationComponent implements OnInit, OnDestroy {

  // Subscription
  subscription: Subscription = new Subscription();
  

  // All created Users
  currentUsers: User[] = [];

  // Enum
  enum = Enum;

  // Reactive forms
  formGroup: FormGroup;


  constructor(
    private _usersService: UsersService,
    private formBuilder: FormBuilder,
    private _title: Title
  ) {
    this.formGroup = this.formBuilder.group({})
  }



  /* ** */
  /* Life Cycles */
  /* ** */

  ngOnInit(): void {
    this._title.setTitle(`${Enum.tabPrefixName}${Enum.componentName}`);

    this.initForm();

    this.subscription = this._usersService.users.subscribe({
      next: (users: User[]) => {
        this.currentUsers = users;
      } 
    });
  }

  ngOnDestroy (): void {
    this.subscription.unsubscribe();
  }


  /* ** */
  /* Component Functions */
  /* ** */

  // Init the reactive form group
  initForm (): void {
    this.formGroup = this.formBuilder.group( {
      valueToSearch: [null],
    });
  }
}
