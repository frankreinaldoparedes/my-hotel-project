// Angular Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing
import { TableInformationRoutingModule } from './table-information-routing.module';

// App Modules
import { TableInformationComponent } from './table-information-component/table-information.component';

// Angular Material Modules
import {MatTableModule} from '@angular/material/table';

// Shared Module
import { SharedModule } from 'src/app/shared/shared.module';

// Pipes
import { FilterPipe } from './pipes/filter.pipe';


@NgModule({
  declarations: [
    TableInformationComponent,
    FilterPipe
  ],
  imports: [
    CommonModule,
    TableInformationRoutingModule,
    MatTableModule,
    SharedModule
  ]
})
export class TableInformationModule { }
