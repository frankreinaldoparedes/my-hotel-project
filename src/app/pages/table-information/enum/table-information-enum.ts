export enum TableInformationEnum {
    // HTML
    title = 'Created Users',
    id = 'ID',
    name = 'Name',
    lastname = 'Lastname',
    email = 'Email',
    createdAt = 'Created At',
    emptyTable = '< We dont have registered users >',
    button = 'Go to Sign In',


    // TS
    componentName = 'Users Table',
    tabPrefixName = 'My Hotel - ',
}