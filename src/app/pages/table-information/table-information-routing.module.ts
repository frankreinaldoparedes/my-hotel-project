import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TableInformationComponent } from './table-information-component/table-information.component';

const routes: Routes = [
  {
    path: '',
    component: TableInformationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TableInformationRoutingModule { }
