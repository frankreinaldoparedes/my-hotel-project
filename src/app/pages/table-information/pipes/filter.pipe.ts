// Angular imports
import { Pipe, PipeTransform } from '@angular/core';

// Types
import { User } from 'src/app/shared/types/user.type';


@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(users: User[], searchedValue: string): User[] {

    if ( users.length === 0 ) {
      return users;
    }

    if ( !searchedValue || searchedValue === '') {
      return users;
    }

    searchedValue = searchedValue.toLowerCase();
    
    return  users.filter( (user: User) => 
      user.lastName?.toLowerCase().includes(searchedValue) ||
      user.email?.toLowerCase().includes(searchedValue)    || 
      user.name?.toLowerCase().includes(searchedValue)     ||
      String(user.id)?.includes(searchedValue)
    );

  }
}