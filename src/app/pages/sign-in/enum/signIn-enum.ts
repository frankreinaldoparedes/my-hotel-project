export enum SignInEnum {
    // HTML
    dontHaveAnAccount = 'I do not have an account',
    
    // TS
    componentName = 'Sign In',
    tabPrefixName = 'My Hotel - ',
    userDosntExist = 'The user does not exist',
    tableRoute = 'users-table'
}