// Angular imports
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

// Tab Title
import { Title } from '@angular/platform-browser';

// Services
import { UsersService } from 'src/app/core/users/users.service';
import { SharedService } from 'src/app/shared/shared.service';
import { ToastrService } from 'ngx-toastr';

// Enum
import { SignInEnum as Enum } from '../enum/signIn-enum';

// Types
import { User, UserDto } from 'src/app/shared/types/user.type';

import { Subject, takeUntil } from 'rxjs';
import { Router } from '@angular/router';



@Component({
  selector: 'sign-in',
  templateUrl: './sign-in.component.html',
  styles: ['button { background-color: #55b0d6; color: #fff; }']
})
export class SignInComponent implements OnInit, AfterViewInit, OnDestroy {

  // Subscriptions
  private _unsubscribeAll: Subject<any> = new Subject<any>();

  formGroup: FormGroup;

  enum = Enum;
  

  constructor(
    private _sharedService: SharedService,
    private _usersService: UsersService,
    private formBuilder: FormBuilder,
    private toast: ToastrService,
    private _router: Router,
    private title: Title,
  ) { 
    this.formGroup = this.formBuilder.group({})
  }



  /* ** */
  /* Life Cycles */
  /* ** */

  ngOnInit (): void {
    // Set the title of the current tab
    this.title.setTitle(`${Enum.tabPrefixName}${Enum.componentName}`);

    this.initForm();
  }

  ngAfterViewInit (): void {
    // Set the value of the Observable
    this._sharedService.formTitleValue = Enum.componentName;
  }

  ngOnDestroy (): void {
    this._unsubscribeAll.next(null);

    this._unsubscribeAll.complete();
  }



  /* ** */
  /* Component Functions */
  /* ** */

  // Init the reactive form group
  initForm (): void {
    this.formGroup = this.formBuilder.group( {
      email: [null, [
        Validators.required,
        Validators.email
      ]],
      password: [null, Validators.required],
    });
  }

  // Method that validate the information in the inputs
  // To save the information
  onSubmit (): void {
    if ( this.formGroup.valid ) {
      const user: UserDto = this.transformData();

      this.saveInformation(user);
    }    
    else {
      this.formGroup.markAllAsTouched();
    }
  }

  // Transform de data from the reactive form into an object to be saved
  transformData (): UserDto {
    let user: UserDto = this._usersService.createEmptyUserDto();

    Object.keys(this.formGroup.controls).map( (controlName: string) => {
      const control: AbstractControl | null = this.formGroup.get(controlName);

      if ( control?.dirty ) {
        user[controlName as keyof UserDto] = control.value;
      }
    });

    return user;
  }

  async saveInformation (user: UserDto): Promise<void> {
    let userExist: boolean = false;

    this._usersService.currentUserValue = user;

    await this._usersService.userExist(user.email)
    .pipe(
      takeUntil<boolean> (this._unsubscribeAll),
    )
    .subscribe({
      next: (exist: boolean) => userExist = exist 
    });

    if( !userExist ) {
      this.toast.error(Enum.userDosntExist)
    }
    else {
      this._router.navigate([Enum.tableRoute]);
    }
  }

  // If the field has error return true, otherwise return false
  fieldHasErrors (controlName: string): boolean | undefined {
    return this.formGroup.get(controlName)?.touched &&  this.formGroup.get(controlName)?.invalid;
  }

}
