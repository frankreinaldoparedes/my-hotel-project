// Angular imports
import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject, map, Observable } from 'rxjs';

// Types
import { User, UserDto } from 'src/app/shared/types/user.type';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public currentUser$: BehaviorSubject<UserDto> = new BehaviorSubject<UserDto>(this.createEmptyUser());

  public users$: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);


  constructor() { }
  

  /*
    Getter and Setter for the current user
  */
  get currentUser (): Observable<UserDto> {
    return this.currentUser$.asObservable();
  }
  
  set currentUserValue (currentUser: UserDto) {
    this.currentUser$.next(currentUser);
  }
  

  /*
    Getter and Setter for the users
  */
  get users (): Observable<User[]> {
    return this.users$.asObservable();
  }
  
  set usersValue (currentUsers: User[]) {
    this.users$.next(currentUsers);
  }


  /*
    Verify if an user with the current email exist
  */
  userExist (email: string | undefined): Observable<boolean> {
    return this.users.pipe(

      map<User[], boolean> ( (user: User[]) => {
        if(user.length === 0) {
          return false;
        }

        const userExist: number = user.findIndex( (user: User) => user.email === email);

        if (userExist === -1) {
          return false;
        }

        return true;

      }),
    );
  }
  

  /*
   Creates an undefined user 
  */ 
  createEmptyUser (): User {
    return {
      id: undefined,
      name: undefined,
      lastName: undefined,
      email: undefined,
      password: undefined,
      createdAt: new Date()
    };
  }

  createEmptyUserDto (): UserDto {
    return {
      email: undefined,
      password: undefined,
    };
  }
  
}
