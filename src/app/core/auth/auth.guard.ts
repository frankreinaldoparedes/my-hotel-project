// Angular imports
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

// Rxjs
import { catchError, Observable, of, Subject, switchMap, timer, takeUntil } from 'rxjs';

// Environment
import { environment } from 'src/environments/environment';

// Services
import { UsersService } from 'src/app/core/users/users.service';
import { AuthService } from './auth.service';
import { ToastrService } from 'ngx-toastr';

// Types
import { UserDto } from 'src/app/shared/types/user.type';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private _usersService: UsersService,
    private _authService: AuthService,
    private _toast: ToastrService
  ) {
  }

  private _unsubscribeAll: Subject<any> = new Subject<any>();


  canActivate (): Observable<boolean> {
    let currentUser: UserDto = this._usersService.createEmptyUserDto();

    this._usersService.currentUser
    .pipe(
      takeUntil<UserDto> (this._unsubscribeAll)
    )
    .subscribe({
      next: (user: UserDto) => currentUser = user
    });

    return this._authService.validUser(currentUser)
    .pipe(
      catchError<any, Observable<boolean>>( () => {
        this.redirect();

        return of(false);
      }),

      switchMap<boolean, Observable<boolean>>( (validUser: boolean) => {
        if ( !validUser )  {
          this._toast.error('Invalid User or Password');

          timer(3000).subscribe({
            next: () => this.redirect()
          });

          this._unsubscribeAll.next(null);
          this._unsubscribeAll.complete();

          return of(false);
        }

        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();

        return of(true);
      })
      
    );
  }

  redirect (): void {
    const urlToRedirect: string = `${environment.appUrl}/sign-in`;

    window.location.replace(urlToRedirect);
  }
  
}
