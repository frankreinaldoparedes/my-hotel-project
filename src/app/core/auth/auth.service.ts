// Angular imports
import { Injectable } from '@angular/core';

// Rxjs
import { map, Observable } from 'rxjs';

// Types
import { User, UserDto } from 'src/app/shared/types/user.type';

// Services
import { UsersService } from '../users/users.service';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor (
    private _usersService: UsersService
  ) {}

  /*
    Verify if an user with the current email exist
  */
  validUser (currentUser: UserDto): Observable<boolean> {
    return this._usersService.users.pipe(

      map<User[], boolean> ( (user: User[]) => {
        if(user.length === 0) {
          return false;
        }

        const userExist: number = user.findIndex( (user: User) => user.email === currentUser.email && user.password === currentUser.password);

        if (userExist === -1) {
          return false;
        }

        return true;

      }),
      
    );
  }
    
}
