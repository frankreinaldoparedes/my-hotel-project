// Angular imports
import { Component, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';

// Services
import { SharedService } from 'src/app/shared/shared.service';

// Rxjs
import { Subscription } from 'rxjs';


@Component({
  selector: 'layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {

  subscriber: Subscription = new Subscription();

  title: string = '';


  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _sharedService: SharedService
  ) { }


  
  /* ** */
  /* Life Cycles */
  /* ** */

  ngOnInit(): void {

    /* Listening Changes to set the value of the title */
    this.subscriber = this._sharedService.formTitle.subscribe({
      next: (title: string) => {
        this.title = title;

        this._changeDetectorRef.detectChanges();
      }
    });

  }

  ngOnDestroy(): void {
    this.subscriber.unsubscribe();
  }
}
