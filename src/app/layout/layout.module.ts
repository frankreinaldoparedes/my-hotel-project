// Angular imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Project Modules
import { LayoutComponent } from './layout-component/layout.component';

// Shared Module
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    LayoutComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    LayoutComponent
  ]
})
export class LayoutModule { }
