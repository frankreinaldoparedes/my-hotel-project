// Angular imports
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

// Guards
import { AuthGuard } from './core/auth/auth.guard';

// Components
import { LayoutComponent } from './layout/layout-component/layout.component';


/* Routes */
const routes: Routes = [

  /* An empty route will redirect to sign-in Page */
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'sign-in', 
  },


  /* Register and Login Routes */
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'register',
        pathMatch: 'full',
        loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterModule )
      },
      {
        path: 'sign-in',
        pathMatch: 'full',
        loadChildren: () => import('./pages/sign-in/sign-in.module').then( m => m.SignInModule )
      }
    ]
  },


  /* Users table */
  {
    path: 'users-table',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/table-information/table-information.module').then( m => m.TableInformationModule )
  },


  /* Any other route will redirect to sign-in Page */
  {
    path: '**',
    redirectTo: 'sign-in', 
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
