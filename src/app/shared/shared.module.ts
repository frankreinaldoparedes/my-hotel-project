// Angular Modules
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

// Reactive forms
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Angular Material Modules
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';

// Shared App Modules
import { InputModule } from './input/input.module';


const sharedMaterialModules: any[] = [
  MatButtonModule,
  MatDividerModule
]

const sharedAngularModules: any[] = [
  ReactiveFormsModule,
  RouterModule,
  FormsModule,
]

const sharedAppModules: any[] = [
  InputModule
]


@NgModule({
  imports: [
    // Modules
    [...sharedAngularModules],
    CommonModule,

    // Angular Material Modules
    [...sharedMaterialModules],

    // Shared App Modules
    [...sharedAppModules],
  ],
  exports: [
    // Modules
    [...sharedAngularModules],
    
    // Angular Material Modules
    [...sharedMaterialModules],

    // Shared App Modules
    [...sharedAppModules],
  ]
})
export class SharedModule { }
