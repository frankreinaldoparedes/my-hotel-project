export enum InputEnum {
    // HTML
    invalid = 'Invalid field',
    
    // TS
    number = 'number',
    empty = '',
}