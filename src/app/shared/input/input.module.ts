// Angular Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { InputComponent } from './input-component/input.component';

// Angular Material Modules
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    InputComponent
  ],
  imports: [
    // Angular Modules
    CommonModule,

    // Angular Material Modules
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
  ],
  exports: [
    InputComponent
  ]
})
export class InputModule { }
