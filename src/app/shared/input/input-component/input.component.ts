// Angular imports
import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

// Enum
import { InputEnum as Enum } from '../enum/input-enum';


@Component({
  selector: 'input-myHotel',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputComponent,
      multi: true
    },
  ]
})
export class InputComponent implements OnInit, ControlValueAccessor {

  // Inputs
  @Input() placeholder: string = '';
  @Input() errorMesage: string = '';
  @Input() type: string = 'text';
  @Input() label: string = '';
  
  @Input() hasError: boolean | undefined = false;
  @Input() isRequired: boolean = false;
  @Input() prefix: string = '';
  @Input() suffix: string = '';


  enum = Enum;


  // Functions
  onChangeFn: Function = (_: any) => {};
  onTouch = () => {};

  currentValue: string | number | undefined = undefined;
  isDisabled: boolean = false;

  constructor () { }

  /* ** */
  /* Life Cycles */
  /* ** */

  ngOnInit (): void {
    this.setPlaceholder();

    this.setLabel();

    this.setValue();
  }


  /* ** */
  /* Value Accessor Functions */
  /* By implementing "Value Accessor" we can bind the input field to a Reactive Form in the parent */
  /* ** */

  writeValue (value: string | number | undefined): void {
    if ( value ) {
      this.currentValue = value;
    }
  }

  registerOnChange (fn: any): void {
    this.onChangeFn = fn;
  }

  registerOnTouched (fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState  (state: boolean): void {
    this.isDisabled = state;
  }


  /* ** */
  /* Component Functions */
  /* ** */


  // Change the value of the reactive form control
  change ($event: any): void {
    let value: string | number = $event.target.value;

    if (this.type === Enum.number) {
      value = Number(value);
    }

    this.onChangeFn($event.target.value)

    this.onTouch();
  }


  // If the placeholder does not exist, it is assigned the value of the label
  setPlaceholder (): void {
    if ( ( !this.placeholder || this.placeholder === Enum.empty ) && ( this.label && this.label !== Enum.empty ) ) {
      this.placeholder = this.label;
    }
  }

  // If the label does not exist, it is assigned the value of the placeholder
  setLabel (): void {
    if ( ( !this.label || this.label === Enum.empty ) && ( this.placeholder && this.placeholder !== Enum.empty ) ) {
      this.label = this.placeholder;
    }
  }

  // If the value does not exist, we assign a empty string
  setValue (): void {
    if (!this.currentValue) {
      this.currentValue = Enum.empty;
    }
  }
}
