import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public formTitle$: BehaviorSubject<string> = new BehaviorSubject<string>('');


  constructor() { }

  /*
    Getter and Setter to set the form title
  */
  get formTitle (): Observable<string> {
    return this.formTitle$.asObservable();
  }

  set formTitleValue (newTitle: string) {
    this.formTitle$.next(newTitle);
  }
}
