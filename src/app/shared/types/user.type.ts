export type User = {
    id: number | undefined;
    name: string | undefined;
    lastName?: string | undefined;
    email: string | undefined;
    password?: string | undefined;
    createdAt: Date | undefined;
}

export type UserDto = Omit< User, 'id' | 'name' | 'createdAt' >;